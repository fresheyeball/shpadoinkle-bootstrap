{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}


module Shpadoinkle.Bootstrap where


import           Shpadoinkle
import           Shpadoinkle.Html
import           Shpadoinkle.Html.TH.CSS


$(extractNamespace "./bootstrap.css")
